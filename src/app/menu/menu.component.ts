import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { flyInOut, expand } from '../animations/app.animations';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host:{
    '[@flyInOut]': 'true',
    'style': 'display:block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class MenuComponent implements OnInit {

  dishes: Dish[];
  errMess: string;
  
 // selectedDish: Dish;

  constructor(private dishService: DishService,
    @Inject('BaseURL') private BaseURL  //Because it's a value not a service (needed for the image)
    ) { }

  //change .then for .subscribe to use Observables
  ngOnInit() {
    this.dishService.getDishes() //Reconfigurying for accept promises
      .subscribe(dishes => this.dishes = dishes, //first funcion if there is a value..
        errmess => this.errMess = <any>errmess); //second function that will be call wen the error occurs (the subscribe method provide a function to manage errors)
  }

 /* onSelect(dish: Dish) {
    this.selectedDish = dish;
  }*/


}

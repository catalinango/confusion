import { Injectable, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
//import { DISHES } from '../shared/dishes';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProcessHTTPMsgService } from './process-httpmsg.service';


@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private http: HttpClient,
    @Inject('BaseURL') private baseURL: string,
    private processHTTPMsgService: ProcessHTTPMsgService
    ) { }

  getDishes(): Observable<Dish[]> { //Promise<Dish[]> remove to use observables
    //Using Promises
    //return Promise.resolve(DISHES); //We configured later the other part... when we are working with some data base
    
    //Simulate server latency with 2 second delay
    //return new Promise (resolve => {
      //setTimeout(() => resolve(DISHES), 2000); 
    //});

    //Using observables
      //return of(DISHES).pipe(delay(2000));   //.toPromise() Remove to use observables
      return this.http.get<Dish[]>(this.baseURL + 'dishes')
        .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  
  getDish(id: string):  Observable<Dish> { // Promise<Dish> remove to use observables

    //return Promise.resolve(DISHES.filter((dish) => (dish.id === id))[0]);
    /*return new Promise (resolve => {
      setTimeout(() => resolve(DISHES.filter((dish) => (dish.id === id))[0]), 2000); 
    }); */

     //Using observables
     //return of(DISHES.filter((dish) => (dish.id === id))[0]).pipe(delay(2000)); //.toPromise() Remove to use observables
     return this.http.get<Dish>(this.baseURL + 'dishes/' + id)
      .pipe(catchError(this.processHTTPMsgService.handleError));
     
  }

  getFeaturedDish():  Observable<Dish> {  // Promise<Dish> remove to use observables
    //return Promise.resolve(DISHES.filter((dish) => dish.featured)[0]);
   /* return new Promise (resolve => {
      setTimeout(() => resolve(DISHES.filter((dish) => dish.featured)[0]), 2000);
    }); */

     //Using observables - returns promises from observables...
     //return of(DISHES.filter((dish) => dish.featured)[0]).pipe(delay(2000)); //.toPromise() Remove to use observables
     return this.http.get<Dish[]>(this.baseURL + 'dishes?featured=true')
      .pipe(map (dishes => dishes[0]))
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getDishIds(): Observable<string[] | any> {
    //return of(DISHES.map(dish => dish.id));
    return this.getDishes().pipe(map(dishes => dishes.map(dish => dish.id)))
      .pipe(catchError(error => error));
  }

  putDish(dish: Dish): Observable<Dish> {
    //Inform the server what will be send
    const httpOptions = { 
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.put<Dish>(this.baseURL + 'dishes/' + dish.id, dish, httpOptions)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
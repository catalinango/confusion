import { Injectable, Inject } from '@angular/core';
import { Leader } from '../shared/leader';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private http: HttpClient,
    @Inject('BaseURL') private baseURL: string,
    private processHttpMsgService: ProcessHTTPMsgService
  ) { }

  getLeaders(): Observable<Leader[]> {
      return this.http.get<Leader[]>(this.baseURL + 'leadership')
        .pipe(catchError(this.processHttpMsgService.handleError));
  }

  getLeader(id: string): Observable<Leader> {
    return this.http.get<Leader>(this.baseURL + `leadership/${id}`)
      .pipe(catchError(this.processHttpMsgService.handleError));
  }

  getFeaturedLeader(): Observable<Leader> {
    return this.http.get<Leader[]>(this.baseURL + 'leadership?featured=true')
      .pipe(map(leaders => leaders[0]))
      .pipe(catchError(this.processHttpMsgService.handleError));
  }

}

import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../shared/comment';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { visibility, flyInOut, expand } from '../animations/app.animations';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],host:{
    '[@flyInOut]': 'true',
    'style': 'display:block;'
  },
  animations: [ 
    visibility(),
    flyInOut(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {
  
  dish: Dish;
  dishcopy: Dish;
  errMess: string;
  dishIds: string[];
  prev: string;
  next: string;
  @ViewChild('cform') commentFormDirective; //Ensure that your Form is going to reset to the inicial values
  commentForm: FormGroup;
  comment: Comment;
  visibility = 'shown';

  formErrors = {
    'author': '',
    'comment': ''    
  };

  validationMessages = {
    'author': {
      'required': 'Your name is required',
      'minlength': 'Your name must be at least 2 characters long'
    },
    'comment': {
      'required': 'Comment is required'
    }
  };

  constructor(
    private dishService: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL  //Because it's a value not a service (needed for the image)
    ) {
      this.createForm();
     }

  ngOnInit() {
    
    /*const id = this.route.snapshot.params['id']; //remove to use observable
    this.dishservice.getDish(id)
      .subscribe(dish => this.dish = dish);
    */
   this.dishService.getDishIds()
      .subscribe((dishIds) => this.dishIds = dishIds);

    //params is the observable... we map the params into another observable call dish, fetching the id value.
    this.route.params
      .pipe(switchMap(
        (params: Params) => { 
          this.visibility = 'hidden'; 
          return this.dishService.getDish(params['id']); 
        }))
      .subscribe(
        dish => { 
          this.dish = dish; 
          this.dishcopy = dish; 
          this.setPrevNext(dish.id); 
          this.visibility = 'shown';
        },
        errmess => this.errMess = <any>errmess 
      );
  }

  createForm() {
    this.commentForm = this.fb.group({
      rating: 5,
      author: ['', [Validators.required, Validators.minLength(2)]],
      comment: ['', Validators.required],
      date:''
    });

    //Subscribing to observable
    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }

  onValueChanged(data?:any) {
    if (!this.commentForm) { 
      return;
    }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        //clear previous error message (if any) - from validations documentation
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) { //revisar!!
          const messages = this.validationMessages[field];
          for (const key  in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + '';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.comment = this.commentForm.value; //Because they have the same structure.
    //this.comment.date = (new Date()).toISOString();
    console.log(this.comment);
    // this.dish.comments.push(this.comment);
    this.dishcopy.comments.push(this.comment);
    this.dishService.putDish(this.dishcopy)
      .subscribe(
        dish => { 
          this.dish = dish; 
          this.dishcopy = dish; 
        },
        errmess => { 
          this.dish = null; 
          this.dishcopy = null; 
          this.errMess = <any>errmess; 
        });
    this.commentFormDirective.resetForm();
    this.commentForm.reset({
      rating:  5,
      author: '',
      comment: '',
      date:''
    });
  } 
  
  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

}
